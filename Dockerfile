FROM ubuntu:latest
MAINTAINER Connor Goodwolf <connor.goodwolf@wolfeon.com>

# Set up the environment
RUN apt-get update
RUN apt-get -y install curl gcc libc6-dev libevent-dev libsasl2-dev make

# Add the service account and group to the system
RUN groupadd --system memcache
RUN useradd --system --gid memcache memcache

# Download and configure memcached
USER memcache
WORKDIR /tmp
RUN curl -J -O -L http://memcached.org/latest
RUN tar zxf latest
RUN BUILD_DIR=$(tar tf latest|head -1) \
    && cd $BUILD_DIR \
    && ./configure --enable-sasl --enable-sasl-pwdb --enable-64bit \
    && make

# Install memcached
USER root
RUN BUILD_DIR=$(tar tf latest|head -1) \
    && cd $BUILD_DIR \
    && make install

# Clean up the environment
RUN dpkg --purge curl gcc libc6-dev libevent-dev libsasl2-dev make
RUN rm -rf /var/lib/apt/lists/*
RUN rm -rf /tmp/latest /tmp/memcache*

EXPOSE 11211

# Tell Docker how to run memcached
USER memcache
WORKDIR /
CMD ["memcached"]
